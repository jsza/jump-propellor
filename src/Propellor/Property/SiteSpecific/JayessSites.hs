module Propellor.Property.SiteSpecific.JayessSites where
import Propellor.Base
import Propellor.Property.User (homedir)

import System.Posix.Files


accountForUid :: User -> String -> Property DebianLike
accountForUid user@(User u) uid = tightenTargets $ check nohomedir go
    `describe` ("account for " ++ u)
  where
    nohomedir = isNothing <$> catchMaybeIO (homedir user)
    go = cmdProperty "adduser"
        [ "--disabled-password"
        , "--gecos", ""
        , "--uid", uid
        , u
        ]


-- | Ensures that a file/dir has the specified owner and group.
ownerGroupRecursive :: FilePath -> User -> Group -> Property UnixLike
ownerGroupRecursive f (User owner) (Group group) = p `describe` (f ++ " owner " ++ og)
  where
    p = cmdProperty "chown" ["-R", og, f] `assume` MadeChange
    og = owner ++ ":" ++ group


symbolicLink :: FilePath -> FilePath -> Property DebianLike
symbolicLink target link = check ((/= Just target) <$> tryWhenExists (readSymbolicLink link)) $
    property (link ++ " symlinked to " ++ target) $
    makeChange (tryWhenExists (removeLink link) >> createSymbolicLink target link)
