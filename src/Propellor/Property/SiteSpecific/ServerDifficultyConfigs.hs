{-# LANGUAGE QuasiQuotes #-}

module Propellor.Property.SiteSpecific.ServerDifficultyConfigs where

import Text.RawString.QQ


data ServerDifficulty
    = Beginner
    | Advanced
    | Default
    | Rookie
    | Rank3000
    | Rank500
    | Rank200
    | Rank100
    | Rank50

serverDifficultyConfig :: ServerDifficulty -> String

serverDifficultyConfig Beginner = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "1"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "0"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "240"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "120"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Advanced = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "2"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "0"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "1440"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "480"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Default = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "0"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "0"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "240"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "120"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Rookie = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "0"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "1"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "1"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "0"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "30"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "20"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Rank3000 = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "0"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "3000"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "1440"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "480"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Rank500 = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "0"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "500"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "1440"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "480"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Rank200 = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "0"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "200"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "1440"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "480"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Rank100 = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "0"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "100"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "1440"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "480"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]

serverDifficultyConfig Rank50 = [r|// Tempus Settings

// Default Value: 0
// Disable saving runs
   tempus_disable_saving "0"


// Default Value: 0
// 0: Default, 1: Beginner, 2: Advanced
   tempus_server_mode "0"


// Default Value: "6"
// Prevent voting for the last X maps.
   tempus_nomination_exclude "6"


// Default Value: "0"
// Allow overriding the map list.
   tempus_override_maplist "0"


// Default Value: "0"
// Restrict players above this rank from joining.
   tempus_restrict_rank "50"


// Default Value: 1
// Enable demo compression.
   tempus_compress_demos "1"


// Default Value: 3
// Number of months before expiring a demo
   tempus_demo_expiry "1"


// Default Value: 1
// Enable automatic upload of WR demos.
   tempus_enable_autoupload "1"


// Default Value: 1
// Enable daily restart
   tempus_daily_restart "1"


// Default Value: 0.7
// Percentage required to extend map
   tempus_extend_percentage "0.7"


// Default Value: 120
// Maximum extend time (in minutes) for !ve
   tempus_extend_limit "1440"


// Default Value: 1
// Enable map downloader
   tempus_enable_map_downloader "0"


// Default Value: 0.7
// Percentage required to trigger RTV
   tempus_rtv_percentage "0.7"


// Default Value: 60
// Maximum extend time (in minutes) for RTV
   tempus_rtv_max_extend "480"


// Default Value: 1
// 0: Disabled, 1: Enabled
   tempus_irc_enable "1"
|]
