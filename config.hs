-- This is the main configuration file for Propellor, and is used to build
-- the propellor program.

import Control.Applicative ((<*>), (<$>))
import Propellor
--import Propellor.CmdLine
import Propellor.Property.Scheduled
import qualified Propellor.Property.File as File
import qualified Propellor.Property.Apt as Apt
import qualified Propellor.Property.Ssh as Ssh
import qualified Propellor.Property.Cron as Cron
import qualified Propellor.Property.Sudo as Sudo
import qualified Propellor.Property.Systemd as Systemd
import qualified Propellor.Property.User as User
import qualified Propellor.Property.Hostname as Hostname
import qualified Propellor.Property.Locale as Locale
import qualified Propellor.Property.Docker as Docker
import           Propellor.Property.Bootstrap

import qualified Propellor.Property.SiteSpecific.JayessSites as JayessSites
import Propellor.Property.SiteSpecific.ServerDifficultyConfigs

import Utility.FileMode


main :: IO ()
main = defaultMain hosts


data Region = USEast | USWest | SouthAmerica | Europe | Asia | Australia | MiddleEast | Africa | World
regionToString :: Region -> String
regionToString USEast = "0"
regionToString USWest = "1"
regionToString SouthAmerica = "2"
regionToString Europe = "3"
regionToString Asia = "4"
regionToString Australia = "5"
regionToString MiddleEast = "6"
regionToString Africa = "7"
regionToString World = "255"


-- The hosts propellor knows about.
-- Edit this to configure propellor!
hosts :: [Host]
hosts = [ jump_AU_AWS
        , jump_US_DevServer
        , jump_AU_Rob
        , jump_AU_Rob_Vultr
        , jump_AU_Hona
        , jump_AU_Adelaide
        , jump_RU
        , jump_SG
        , jump_US1
        , jump_US4
        , jump_NY
        , jump_EU
        , jump_France1
        , jump_ZA
        , jump_Seattle
        , jump_Dallas
        , jump_NZ
        , jump_KR
        , jump_FI
        , jump_AR
        , jump_SE
        , jump_BR
        , jump_LosAngeles2
        ]


jump_AU_AWS :: Host
jump_AU_AWS = host "au3.tempus.xyz" $ props
    & secureSystem (Stable "jessie") X86_64
    & ipv4 "52.64.129.109"
    & gameServer "AU3" "Jump (AU) Expert | Tempus Network" Australia
                 "jump_apex" "24" "27015" "27014" False Advanced


jump_AU_Rob :: Host
jump_AU_Rob = host "au.tempus.xyz" $ props
    & secureSystem (Stable "jessie") X86_64
    & ipv4 "119.252.191.125"
    & gameServer "AU1" "Jump (AU) Beginner | Tempus Network" Australia
                 "jump_eons_b2" "24" "27015" "27014" False Beginner
    & gameServer "AU2" "Jump (AU) Advanced | Tempus Network" Australia
                 "jump_cheval" "16" "27025" "27024" False Advanced


jump_AU_Rob_Vultr :: Host
jump_AU_Rob_Vultr = host "au1-2.tempus.xyz" $ props
    & secureSystem (Stable "bullseye") X86_64
    & ipv4 "45.32.243.18"
    & gameServer "AU1" "Jump (AU) All Maps | Tempus Network" Australia
                 "jump_eons_b2" "24" "27015" "27014" False Default
    & gameServer "AU2" "Jump (AU) Advanced | Tempus Network" Australia
                 "jump_cheval" "16" "27025" "27024" False Advanced
    & gameServerMemoryLimit "AU1" 943718400 27014
    & gameServerMemoryLimit "AU2" 943718400 27024


jump_AU_Hona :: Host
jump_AU_Hona = host "au3-4.tempus.xyz" $ props
    & secureSystem (Stable "buster") X86_64
    & ipv4 "139.180.172.245"
    & gameServer "AU3" "Jump (AU) | Rank 200 Only | Tempus Network" Australia
                 "jump_eons_b2" "24" "27015" "27014" False Rank200
    & gameServer "AU4" "Jump (AU) Advanced #2 | Tempus Network" Australia
                 "jump_cheval" "16" "27025" "27024" False Advanced
    & gameServerMemoryLimit "AU3" 943718400 27014
    & gameServerMemoryLimit "AU4" 943718400 27024


jump_AU_Adelaide :: Host
jump_AU_Adelaide = host "adelaide.tempus.xyz" $ props
    & secureSystem (Stable "buster") X86_64
    & ipv4 "103.25.57.21"
    & gameServer "AU6" "Jump (Adelaide) | All Maps | Tempus Network" Australia
                 "jump_apex_b1" "24" "27015" "27014" False Default
    & gameServerMemoryLimit "AU6" 943718400 27014


jump_NZ :: Host
jump_NZ = host "nz.tempus.xyz" $ props
    & secureSystem (Stable "stretch") X86_64
    & ipv4 "163.47.21.18"
    & gameServer "NZ1" "Jump (NZ) All Maps | Tempus Network" Australia
                 "jump_eons_b2" "24" "27015" "27014" False Default
    & gameServerMemoryLimit "NZ1" 943718400 27014


jump_RU :: Host
jump_RU = host "ru1.tempus.xyz" $ props
    & ruSystem (Stable "bullseye") X86_64
    & ipv4 "109.170.16.98"
    & gameServer "RU1" "RG #15 - Jump (No rank limit) | Tempus Network" Europe
                 "jump_adventure" "16" "27050" "27049" False Default
    & gameServer "RU2" "RG #16 - Jump (Rank 500 Only) | Tempus Network" Europe
                 "jump_adventure" "16" "27055" "27054" False Rank500
    & User.accountFor (User "andy")
    & User.lockedPassword (User "andy")
    & Sudo.enabledFor (User "andy")
    & andyKeys (User "andy")
    & andyKeys (User "root")
    & User.accountFor (User "vadim")
    & User.lockedPassword (User "vadim")
    & Sudo.enabledFor (User "vadim")
    & vadimKeys (User "vadim")
    & vadimKeys (User "root")


jump_SG :: Host
jump_SG = host "sg1-2.tempus.xyz" $ props
    & standardSystem (Stable "bullseye") X86_64
    -- ~/.ssh/config
    -- Host sql.asiafortress.com
    -- Port 18004
    -- Host 101.100.183.42
    -- Port 18004
    & ipv4 "101.100.183.42"
    & Ssh.passwordAuthentication True
    & gameServer "SG1" "AsiaFortress.com SG #09 | Jump Beginner #1 | Tempus Network" Asia
                 "jump_eons_b2" "24" "26009" "1337" False Beginner
    & gameServer "SG2" "AsiaFortress.com SG #10 | Jump Advanced #1 | Tempus Network" Asia
                 "jump_cheval" "24" "26010" "1338" False Advanced
    & gameServer "SG3" "AsiaFortress.com SG #11 | Jump Beginner #2 | Tempus Network" Asia
                 "jump_eons_b2" "24" "26011" "1339" False Beginner
    & gameServer "SG4" "AsiaFortress.com SG #12 | Jump Advanced #2 | Tempus Network" Asia
                 "jump_cheval" "24" "26012" "1340" False Advanced


jump_US_DevServer :: Host
jump_US_DevServer = host "dev1.tempus.xyz" $ props
    & secureSystem (Stable "jessie") X86_64
    & ipv4 "74.91.119.210"
    & gameServer "DEV1" "Dev Server" USEast
                 "itemtest" "8" "27015" "27014" True Default


jump_US1 :: Host
jump_US1 = host "us1.tempus.xyz" $ props
    & secureSystem (Stable "stretch") X86_64
    & ipv4 "74.91.113.87"
    & gameServer "US1" "jumpacademy.tf (Chicago) Advanced | Tempus Network" USEast
                 "jump_apex" "24" "27015" "27014" False Advanced
    & gameServer "US3" "jumpacademy.tf (Chicago) Beginner | Tempus Network" USEast
                 "jump_orbital_rc1" "24" "27025" "27024" False Beginner
    & gameServerMemoryLimit "US1" 1363148800 27014
    & gameServerMemoryLimit "US3" 1363148800 27024


jump_US4 :: Host
jump_US4 = host "us4.tempus.xyz" $ props
    & secureSystem (Stable "buster") X86_64
    & ipv4 "162.248.93.239"
    & gameServer "US2" "jump.tf - Advanced (Los Angeles) | Tempus Network" USWest
                 "jump_gateway_final" "24" "27025" "27024" False Advanced
    & gameServer "US4" "jump.tf - Beginners (Los Angeles) | Tempus Network" USWest
                 "jump_beefmas" "24" "27015" "27014" False Beginner
    & gameServerMemoryLimit "US2" 1363148800 27014
    & gameServerMemoryLimit "US4" 1363148800 27024
    & User.accountFor (User "whisker")
    & User.lockedPassword (User "whisker")
    & Sudo.enabledFor (User "whisker")
    & User.hasGroup (User "whisker") (Group "docker")
    & whiskerKeys (User "whisker")


jump_NY :: Host
jump_NY = host "ny.tempus.xyz" $ props
    & secureSystem (Stable "stretch") X86_64
    & ipv4 "192.223.24.193"
    & gameServer "US5" "jump.tf (NY) | All Maps | Tempus Network" USEast
                 "jump_beefmas" "24" "27015" "27014" False Default
    & gameServer "US6" "jump.tf (NY) | Rank 200 Only | Tempus Network" USEast
                 "jump_gateway_final" "24" "27025" "27024" False Rank200
    & gameServer "US10" "jump.tf (NY) | Rank 50 Only | Tempus Network" USEast
                 "jump_remember_beta" "24" "27035" "27034" False Rank50
    & gameServerMemoryLimit "US5" 943718400 27014
    & gameServerMemoryLimit "US6" 943718400 27024
    & gameServerMemoryLimit "US10" 943718400 27034


jump_Seattle :: Host
jump_Seattle = host "seattle.tempus.xyz" $ props
    & secureSystem (Stable "stretch") X86_64
    & ipv4 "74.91.117.195"
    & gameServer "US7" "Echo Jump (Seattle, US) | All Maps | Tempus Network" USEast
                 "jump_apex" "24" "27015" "27014" False Default
    & gameServerMemoryLimit "US7" 943718400 27014


jump_Dallas :: Host
jump_Dallas = host "dallas.tempus.xyz" $ props
    & secureSystem (Stable "buster") X86_64
    & ipv4 "74.91.126.110"
    & gameServer "US8" "jump.tf (Dallas) Jump | All Maps | Tempus Network" USEast
                 "jump_beefmas" "24" "27015" "27014" False Default
    & gameServer "US9" "jump.tf (Dallas) Jump | Rank 100 Only | Tempus Network" USEast
                 "jump_gateway_final" "24" "27025" "27024" False Rank100
    & gameServerMemoryLimit "US8" 1363148800 27014
    & gameServerMemoryLimit "US9" 1363148800 27024


jump_EU :: Host
jump_EU = host "germany.tempus.xyz" $ props
    & standardSystem (Stable "jessie") X86_64
    & ipv4 "85.10.208.200"
    & gameServer "EU1" "jump.tf (Europe) Beginner | Tempus Network" Europe
                 "jump_junk_solly_b1" "24" "27015" "27014" False Beginner
    & gameServer "EU2" "jump.tf (Europe) Advanced #1 | Tempus Network" Europe
                 "jump_lanc_v3" "24" "27025" "27024" False Advanced
    & gameServer "EU3" "jump.tf (Europe) Advanced #2 | Tempus Network" Europe
                 "jump_jurf2_a1" "16" "27095" "27094" False Advanced


-- jump_France1 :: Host
-- jump_France1 = host "paris.tempus.xyz" $ props
--     & secureSystem (Stable "stretch") X86_64
--     & ipv4 "149.202.65.78"
--     & gameServer "FRA1" "jump.tf (France) | expiring Jul 11 | All Maps #1 | Tempus" Europe
--                  "jump_4starters_rc1" "24" "26015" "26014" False Default
--     & gameServer "FRA2" "jump.tf (France) | expiring Jul 11 | All Maps #2 | Tempus" Europe
--                  "jump_hopstep_a3" "24" "26025" "26024" False Default
--     & gameServer "FRA3" "jump.tf (France) | expiring Jul 11 | Rank 100 Only | Tempus" Europe
--                  "jump_hopstep_a3" "24" "26035" "26034" False Rank100
--     & gameServer "FRA4" "jump.tf (France) | expiring Jul 11 | All Maps #3 | Tempus" Europe
--                  "jump_hopstep_a3" "24" "26045" "26044" False Default


jump_France1 :: Host
jump_France1 = host "france1.tempus.xyz" $ props
    & secureSystem (Stable "bullseye") X86_64
    & ipv4 "149.202.86.218"
    & User.accountFor (User "ai")
    & User.lockedPassword (User "ai")
    & Sudo.enabledFor (User "ai")
    & aiKeys (User "ai")
    & User.accountFor (User "kaptain")
    & User.lockedPassword (User "kaptain")
    & Sudo.enabledFor (User "kaptain")
    & kaptainKeys (User "kaptain")
    & kaptainKeys (User "root")
    & gameServer "FRA1" "jump.tf (France) All Maps #1 | Tempus Network" Europe
                 "jump_4starters_rc1" "24" "26015" "26014" False Default
    & gameServer "FRA2" "jump.tf (France) All Maps #2 | Tempus Network" Europe
                 "jump_hopstep_a3" "24" "26025" "26024" False Default
    & gameServer "FRA3" "jump.tf (France) Rank 100 Only | Tempus Network" Europe
                 "jump_hopstep_a3" "24" "26035" "26034" False Rank100
    & gameServer "FRA4" "jump.tf (France) Rank 50 Only | Tempus Network" Europe
                 "jump_hopstep_a3" "24" "26045" "26044" False Rank50


jump_ZA :: Host
jump_ZA = host "za.tempus.xyz" $ props
    & system (Stable "buster") X86_64
    & ipv4 "129.232.150.23"
    & gameServer "ZA1" "TF2SA Jump (All Maps) | tempus.tf" Africa
                 "jump_4starters_rc1" "16" "27015" "27014" False Default


jump_KR :: Host
jump_KR = host "kr.tempus.xyz" $ props
    & standardSystem (Stable "jessie") X86_64
    & ipv4 "49.247.206.89"
    & gameServer "KR1" "jump.tf | 공식 한국 템퍼스 점프 서버 #1 | Tempus Network" Asia
                 "jump_4starters_rc1" "24" "27015" "27014" False Default
    & gameServer "KR2" "jump.tf | 템퍼스 랭크 3000 제한 | Tempus Network" Asia
                 "jump_4starters_rc1" "24" "27025" "27024" False Rank3000


jump_FI :: Host
jump_FI = host "fi.tempus.xyz" $ props
    & secureSystem (Stable "jessie") X86_64
    & ipv4 "95.216.103.108"
    & gameServer "FI1" "jump.tf (Finland) All Maps #1 | Tempus Network" Europe
                 "jump_4starters_rc1" "24" "27015" "27014" False Default


jump_AR :: Host
jump_AR = host "argentina.tempus.xyz" $ props
    & standardSystem (Stable "buster") X86_64
    & gameServer "AR1" "jump.tf (Argentina) Beginner | Tempus Network" SouthAmerica
                 "jump_4starters_rc1" "12" "27015" "27014" False Beginner
    & gameServer "AR2" "jump.tf (Argentina) Advanced | Tempus Network" SouthAmerica
                 "jump_4starters_rc1" "12" "27020" "27019" False Advanced


jump_SE :: Host
jump_SE = host "sweden.tempus.xyz" $ props
    & secureSystem (Stable "bullseye") X86_64
    & ipv4 "185.243.215.51"
    & User.accountFor (User "waldo")
    & User.lockedPassword (User "waldo")
    & Sudo.enabledFor (User "waldo")
    & waldoKeys (User "waldo")
    & gameServer "SE1" "jump.tf (Sweden) | All Maps #1 | Tempus Network" Europe
                 "jump_aznbob_fixed" "24" "27015" "27014" False Default
    & gameServer "SE2" "jump.tf (Sweden) | All Maps #2 | Tempus Network" Europe
                 "jump_aznbob_fixed" "24" "27020" "27019" False Default
    & gameServer "SE3" "jump.tf (Sweden) | Rank 200 Only | Tempus Network" Europe
                 "jump_aando_b1_tmps" "24" "27025" "27024" False Rank200


jump_BR :: Host
jump_BR = host "br.tempus.xyz" $ props
    & Apt.mirror "http://kvmmgr-sp3.gcorelabs.com:8088/debian"
    & secureSystem (Stable "bullseye") X86_64
    & ipv4 "5.188.238.87"
    & User.accountFor (User "waldo")
    & User.lockedPassword (User "waldo")
    & Sudo.enabledFor (User "waldo")
    & waldoKeys (User "waldo")
    & gameServer "BR1" "jump.tf (Brazil) | All Maps | Tempus Network" SouthAmerica
                 "jump_tholos" "24" "27015" "27014" False Default


jump_LosAngeles2 :: Host
jump_LosAngeles2 = host "la2.tempus.xyz" $ props
    & secureSystem (Stable "bullseye") X86_64
    & ipv4 "104.153.109.119"
    & User.accountFor (User "waldo")
    & User.lockedPassword (User "waldo")
    & Sudo.enabledFor (User "waldo")
    & waldoKeys (User "waldo")
    & gameServer "US11" "jump.tf (Los Angeles) | All Maps #1 | Tempus Network" Europe
                 "jump_something_rc1_fix_v2" "24" "27015" "27014" False Default
    & gameServer "US12" "jump.tf (Los Angeles) | All Maps #2 | Tempus Network" Europe
                 "jump_speed2" "24" "27025" "27024" False Default
    & gameServer "US13" "jump.tf (Los Angeles) | Rank 200 Only | Tempus Network" Europe
                 "jump_rickoconnell" "24" "27035" "27034" False Rank200
    & gameServer "US14" "jump.tf (Los Angeles) | Rank 50 Only | Tempus Network" Europe
                 "jump_the_b7" "24" "27045" "27044" False Rank50


secureSystem :: DebianSuite -> Architecture -> Property (HasInfo + Debian)
secureSystem suite arch = propertyList "secure docker system" $ props
    & standardSystem suite arch
    & Ssh.randomHostKeys
    & Ssh.noPasswords


standardSystem :: DebianSuite -> Architecture -> Property (HasInfo + Debian)
standardSystem suite arch = propertyList "standard system" $ props
    & system suite arch
    & Hostname.sane
    & Hostname.searchDomain


ruSystem :: DebianSuite -> Architecture -> Property (HasInfo + Debian)
ruSystem suite arch = propertyList "ru docker system" $ props
    & system suite arch
    -- & Ssh.randomHostKeys
    & Ssh.noPasswords


system :: DebianSuite -> Architecture -> Property (HasInfo + Debian)
system suite arch = propertyList "standard system" $ props
    & osDebian suite arch
    & bootstrapWith (Robustly Stack)
    & Locale.available "en_US.UTF-8"
    & Apt.stdSourcesList `onChange` Apt.update
    & Apt.unattendedUpgrades
    & Apt.cacheCleaned
    & Apt.removed ["needrestart"]
    & Apt.installed [ "openssh-server"
                    , "openssh-client"
                    , "git"
                    , "ca-certificates"
                    -- SRCDS libs
                    , "conntrack"
                    ]
    & Apt.serviceInstalledRunning "ntp"
    & Ssh.permitRootLogin (Ssh.RootLogin True)
    & Docker.installed
    & Docker.garbageCollected `period` Daily
    & Docker.memoryLimited
    & Apt.installed ["sudo"]
    & propertyList "admin accounts"
    (toProps $
     [ User.accountFor
     , User.lockedPassword
     , setupRevertableProperty . Sudo.enabledFor
     ] <*> admins)
    & propertyList "admin docker access"
    (toProps (flip User.hasGroup (Group "docker") <$> admins))
    & adminKeys (User "root")
    & jayessKeys (User "jayess")
    & User.shellSetTo (User "jayess") "/usr/bin/fish"
    & robKeys (User "rob")
    & User.shellSetTo (User "rob") "/usr/bin/fish"
    & scotchKeys (User "scotch")
    & User.shellSetTo (User "scotch") "/usr/bin/fish"
    & JayessSites.accountForUid (User "steam-docker") "5000"
    & User.lockedPassword (User "steam-docker")
    & adminKeys (User "steam-docker")
    -- Useful utilities
    & Apt.installed [ "htop"
                    , "less"
                    , "curl"
                    , "atool"
                    , "vim"
                    , "fish"
                    , "rsync"
                    ]
    & File.notPresent "/etc/cron.d/propellor"
    & mapUpdater "/srv/tempus_maps"


gameServer :: String -> String -> Region -> String -> String -> String ->
              String -> Bool -> ServerDifficulty -> Property (HasInfo  + DebianLike)
gameServer shortname title region defaultMap maxPlayers port apiPort devServer difficulty = propertyList "game server" $ props
    & defaultServerConfig shortname title region devServer
    & gameService shortname defaultMap maxPlayers port apiPort difficulty


gameServerMemoryLimit :: String ->  Int -> Int -> Property DebianLike
gameServerMemoryLimit shortname limit port = propertyList "gameserver memory limiter" $ props
    & scriptFile `File.hasContent` script
    & scriptFile `File.mode` combineModes (readModes ++ executeModes)
    & Cron.niceJob
      (container ++ " memory check")
      (Cron.Times "* * * * *") (User "root") "/" command

    where
        container = "tf2_" ++ shortname
        scriptFile = "/usr/local/bin/docker_container_memorylimiter"
        script =
          [ "#!/bin/bash"
          , "set -e"
          , "CONTAINER=$1"
          , "HIGH_LIMIT=$2"
          , "LOW_LIMIT=`expr $HIGH_LIMIT - 52428800`"
          , "API_URL=$3"
          , "DOCKERID=`docker inspect -f \"{{ .Id }}\" ${CONTAINER}`"
          , "CGROUP_PATH=\"/sys/fs/cgroup/memory/system.slice/docker-${DOCKERID}.scope/memory.stat\""
          , "if [ ! -f ${CGROUP_PATH} ]; then"
          , "    exit"
          , "fi"
          , "CURRENT=`grep -m1 total_rss ${CGROUP_PATH} | cut -d \" \" -f 2`"
          , "if (( CURRENT >= HIGH_LIMIT )); then"
          , "    curl -X POST -d '{\"message\": \"Running out of memory\", \"delay\": 1800.0}' $API_URL/restart/delayed"
          , "elif (( CURRENT >= LOW_LIMIT )); then"
          , "    curl -X POST $API_URL/restart/mapchange"
          , "fi"
          ]
        command = scriptFile ++ " " ++ container ++ " " ++ (show limit)
                  ++ " http://localhost:" ++ (show port)


gameService :: String -> String -> String -> String -> String -> ServerDifficulty -> Property (HasInfo + DebianLike)
gameService shortname defaultMap maxPlayers port apiPort difficulty = propertyList "game service" $ props
    & File.dirExists serverPath
    & File.dirExists tempusCfgPath
    & (tempusCfgPath ++ "maptype") `File.hasContent` ["jump"]
    -- & File.hasPrivContent (tempusCfgPath ++ "db.yml") hostContext
    -- & File.hasPrivContent (tempusCfgPath ++ "s3.yml") hostContext
    -- & File.hasPrivContent (tempusCfgPath ++ "api.yml") hostContext
    -- & File.hasPrivContent (tempusCfgPath ++ "wamp.yml") hostContext
    & File.hasPrivContent (tempusCfgPath ++ "tempus.yml") hostContext
    & File.dirExists sourcePythonCfgPath
    & File.hasContent (sourcePythonCfgPath ++ "tempus.cfg")
                      (lines sourcePythonCfgContent)
    & JayessSites.ownerGroupRecursive serverPath (User "5000") (Group "docker")
    & File.dirExists "/srv/tempus_maps"
    & JayessSites.symbolicLink ("/srv/tempus_maps") (serverPath ++ "/tf/custom/tempus/maps")

    & ("/etc/systemd/system/" ++ serviceName ++ ".service") `File.hasContent`
        [ "[Unit]"
        , "Description=Tempus TF2 Jump Server (" ++ shortname ++ ")"
        , "After=docker.service"
        , "Requires=docker.service"
        , ""
        , "[Service]"
        , "Restart=always"
        -- , "StandardInput=tty-force"
        , ""
        , "ExecStartPre=-/usr/bin/docker kill " ++ serviceName
        , "ExecStartPre=-/usr/bin/docker rm " ++ serviceName
        -- , "ExecStartPre=/usr/bin/docker pull jayess/tf2-tempus"
        , "ExecStartPre=/usr/bin/docker create --name " ++ serviceName
          ++ " --interactive --tty"
          ++ " --cap-add=sys_nice"
          ++ " --publish " ++ port ++ ":" ++ port ++ "/udp"
          ++ " --env TEMPUS_LOCALAPI_PORT=tcp:8080"
          ++ " --publish 127.0.0.1:" ++ apiPort ++ ":8080"
          ++ " --volume " ++ serverPath ++ ":/srv/srcds"
          ++ " --volume /srv/tempus_maps:/srv/tempus_maps"
          ++ " " ++ imageName
          ++ " -port " ++ port
          ++ " +map " ++ defaultMap
          ++ " -maxplayers " ++ maxPlayers
          ++ " -norestart"
        , "ExecStart=/usr/bin/docker start --attach " ++ serviceName
        , "ExecStartPost=-/bin/sh -c 'sleep 5; /usr/bin/docker inspect -f \"{{ .NetworkSettings.IPAddress }}\" " ++ serviceName ++ "> /run/docker-ip-" ++ serviceName ++ "'"
        , "ExecStopPost=-/usr/bin/docker kill " ++ serviceName
        , "ExecStopPost=-/bin/sh -c 'conntrack -D -r $(cat /run/docker-ip-" ++ serviceName ++ ")'"
        , ""
        , "[Install]"
        , "WantedBy=multi-user.target"
        ]
        `onChange` Systemd.daemonReloaded
    & (serverPath ++ "/tf/addons/sourcemod/configs/core.cfg") `File.hasContent`
        [ "Core"
        , "{"
        , "    \"Logging\"                    \"on\""
        , "    \"LogMode\"                    \"daily\""
        , "    \"ServerLang\"                 \"en\""
        , "    \"PublicChatTrigger\"          \"!\""
        , "    \"SilentChatTrigger\"          \"/\""
        , "    \"SilentFailSuppress\"         \"no\""
        , "    \"PassInfoVar\"                \"_password\""
        , "    \"MenuItemSound\"              \"buttons/button14.wav\""
        , "    \"MenuExitSound\"              \"buttons/combine_button7.wav\""
        , "    \"MenuExitBackSound\"          \"buttons/combine_button7.wav\""
        , "    \"AllowClLanguageVar\"         \"On\""
        , "    \"DisableAutoUpdate\"          \"no\""
        , "    \"ForceRestartAfterUpdate\"    \"yes\""
        , "    \"AutoUpdateURL\"              \"http://update.sourcemod.net/update/\""
        , "    \"DebugSpew\"                  \"no\""
        , "    \"SteamAuthstringValidation\"  \"yes\""
        , "    \"BlockBadPlugins\"            \"yes\""
        , "    \"SlowScriptTimeout\"          \"8\""
        , "    \"FollowCSGOServerGuidelines\" \"yes\""
        , "    \"MinidumpAccount\"            \"76561198154210726\""
        , "}"
        ]
    & Systemd.enabled (serviceName)
    & Systemd.started (serviceName)

    where
        serviceName = "tf2_" ++ shortname
        imageName = "jayess/tf2-tempus"
        serverPath = "/srv/" ++ serviceName
        tempusCfgPath = serverPath ++ "/tf/addons/source-python/plugins/tempus_loader/local/cfg/"
        sourcePythonCfgPath = serverPath ++ "/tf/cfg/source-python/"
        sourcePythonCfgContent = serverDifficultyConfig difficulty


mapUpdater :: String -> Property Linux
mapUpdater mapsPath = propertyList "map updater" $ props
    & File.notPresent "/etc/cron.d/tempus_map_updater"
    & File.dirExists "/srv/tempus_maps"
    & JayessSites.ownerGroupRecursive "/srv/tempus_maps" (User "5000") (Group "5000")
    & ("/etc/systemd/system/" ++ serviceName ++ ".service") `File.hasContent`
        [ "[Unit]"
        , "Description=Tempus Map Updater"
        , "After=docker.service"
        , "Requires=docker.service"
        , ""
        , "[Service]"
        , "Restart=always"
        , ""
        , "ExecStartPre=-/usr/bin/docker kill " ++ serviceName
        , "ExecStartPre=-/usr/bin/docker rm " ++ serviceName
        -- , "ExecStartPre=/usr/bin/docker pull " ++ imageName
        , "ExecStartPre=/usr/bin/docker create --name " ++ serviceName ++ " "
          ++ "--interactive --tty "
          ++ "-v " ++ mapsPath ++ ":/srv/tempus_maps "
          ++ imageName ++ " "
          ++ "--notify-server 'wss://tempus.xyz:9000' "
          ++ "--notify-realm 'jump' "
          ++ "--maps-path '/srv/tempus_maps' "
          ++ "--delete "
          ++ "--tf-level-sounds "
          ++ "weblist "
          ++ "--fetch-url 'http://tempus.site.nfoservers.com/server/maps/' "
        , "ExecStart=/usr/bin/docker start --attach " ++ serviceName
        , "ExecStopPost=-/usr/bin/docker kill " ++ serviceName
        , ""
        , "[Install]"
        , "WantedBy=multi-user.target"
        ]
        `onChange` Systemd.daemonReloaded
    & Systemd.enabled (serviceName)
    & Systemd.started (serviceName)
    where
        serviceName = "map_updater"
        imageName = "jayess/tempus-new-map-updater"


defaultServerConfig :: String -> String -> Region -> Bool -> Property (HasInfo + UnixLike)
defaultServerConfig shortname title region devServer = propertyList "srcds config files" $ props
    & File.dirExists (cfgPath)
    & File.hasPrivContent (cfgPath ++ "server_account.cfg") hostContext
    & (cfgPath ++ "autoexec.cfg") `File.hasContent`
        [ "exec server_account.cfg"
        , "sp plugin load tempus_loader"
        , (if devServer then "tv_enable 0" else "tv_enable 1")
        , "tv_maxclients 0"
        , "sp plugin load noshake"
        ]
    & (cfgPath ++ "server.cfg") `File.hasContent`
        [ "exec tempus_defaults.cfg"
        , "exec tempus_server.cfg"
        , "exec tempus_serverspecific.cfg"
        ]
    & (cfgPath ++ "tempus_defaults.cfg") `File.hasContent`
        [ "sm_cvar mp_waitingforplayers_time 0"
        , "sm_cvar tf_max_health_boost 1"
        , "sm_cvar tf_max_charge_speed 0"
        , "sm_cvar weapon_medigun_chargerelease_rate 0"
        , "sm_cvar weapon_medigun_charge_rate 99999"
        , "sm_cvar tf_invuln_time 0"
        , "sm_cvar sv_airaccelerate 10"
        , "sm_cvar tf_whip_speed_increase 0"
        , "//sm_cvar tf_sticky_airdet_radius 1.0"
        , "//sm_cvar tf_sticky_radius_ramp_time 0"
        , "mp_respawnwavetime 0"
        , "mp_idledealmethod 0"
        , "mp_autoteambalance 0"
        , "mp_teams_unbalance_limit 0"
        , "mp_timelimit 30"
        , "sv_allow_votes 0"
        , "sv_alltalk 1"
        , "sv_hudhint_sound 0"
        , "sv_allow_wait_command 0"
        , "tf_weapon_criticals 0"
        , "tf_avoidteammates_pushaway 0"
        , "tf_use_fixed_weaponspreads 1"
        , (if devServer then "tv_enable 0" else "tv_enable 1")
        , "tv_allow_camera_man 0"
        , "tv_allow_static_shots 0"
        , "tv_name \"SpookyTV\""
        , "// Hostname for server."
        , "rcon_password \"\""
        , "sv_downloadurl \"http://tempus.site.nfoservers.com/server/\""
        , "net_maxfilesize 999"
        , "sv_allowdownload 0"
        , "sv_allowupload 0"
        , "tf_allow_server_hibernation 0"
        , "tf_player_movement_restart_freeze 0"
        , "sv_client_min_interp_ratio 0"
        , "sv_client_max_interp_ratio 2"
        ]
    & (cfgPath ++ "tempus_server.cfg") `File.hasContent`
        [ "hostname \"" ++ title ++ "\""
        , "sv_region " ++ (regionToString region)
        , "sv_password \"\""
        ]

    where
        cfgPath = "/srv/tf2_" ++ shortname ++ "/tf/custom/tempus/cfg/"


admins :: [User]
admins = map User ["jayess", "rob", "scotch"]


jayessKeys :: User -> Property UnixLike
jayessKeys user = propertyList "keys for jayess"
                  . toProps
                  . map (setupRevertableProperty . Ssh.authorizedKey user) $
                  [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiy5Sx5pADzWP9Aq+ecagisuc2jZJaR/DV4PoVWxNAH4HngybzBBUtTL/9BsLcTn5OKNGqc1Kk916PENBPN3sqNQJj1u+OUyibAT8Em/sEfaDZ5ykh++E0/ycKYFs2chXR7fPhe+68hLAMNS3GlKvf5ErmScz3oyDEwR73b00LfABz3rpy7YuxoNiA/PgPv4+5oaULUxo0ysGx+mcoAvrXwQ5u3KHPOKNNzN9E3gF5AhML+qGF5i7T3dYcZ0OsqkEJ4gSRG8PPVmX2rKMI+Ldvh0LI0Xa9fgaEgtC5X38u+0WalEE5EhBv5LUZKRu+9bzkR71jl9kbI86ld/QLYf9Z js@mvp.gg",
                  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsdT1r84ZlfcHyibUUZqrT1SkJ5JNBVd/Uwym4T2L2okUqqet+a+oxzIyclN2/9ay8hdk+Fmh4uro97F0Mu7OxX/K8qsa/C6B1wLWdYB4BrbHxhPs8edklPRVB0QaepYNtUZWvvb+TOekCRaG9KTYatAcd/0hfu6qiAk2RvIOJk0g8o2WWhIKoUUYUrAUuoBQhAhOtfcZGjriIiMWjkX0+NpGx6twpgQvQNYy7w39NrgP0IhnVd1gJw6mSIwCQM0p3ztvxI2x0Iwi8oFSq6UwZnHVIs7b7Gms+rv+Q7yXvCYaYvFBnM3a8VuVc7WFJ7CwEGxqtb+PBneeDzd/z2OeF cardno:000603012235"
                  ]


robKeys :: User -> Property UnixLike
robKeys user = propertyList "keys for rob"
               . toProps
               . map (setupRevertableProperty . Ssh.authorizedKey user) $
               [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAp5Nj72+zRWKLqtOWbGGDoD0mReIbk+0JtCILvzxLgBCmV/xbaJkdtlglvIdO0qbStZNTI7mUtGdympBx0a16cTtHqpyuPgKG8MimfcBWf+0z1WdDWpd9xVsVgKEQoSj4ZomHOsotDTY+2E5eBlZqmXK+rYGRsuQCeFShz1HfN5HDtuk5nekdzipNWvtHIJ9a3lDLLTn6lyuh4pv8OGBb3qYf9ZI6PEzeKbEPMhpIhn4TJeGp/hXMgwp2CTIkEG/6vINF9hfWr34dye107dDV0sRVvWtOTefOCk0YYXwfY+L5ecNJZNL75SjfpBpbjYJZmuaPaVA0/5InDmksWO/8xQ== rsa-key-20140404"
               ]


whiskerKeys :: User -> Property UnixLike
whiskerKeys user = propertyList "keys for whisker"
                   . toProps
                   . map (setupRevertableProperty . Ssh.authorizedKey user) $
                   [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAnUJVcuJxUguNne6QRn9uX3sFxVoUmzJSEJrb63LSvrQmzAHYOA7KY+gq7k7oJ+97kAEK7UeUHZpxdmeJ4gFswV+vYFXGqMnlUl2HJGS5lqxK030jJeFMSfQ8oO0P2rnShS5sMQ6FXWGPsBYD2Bu//14RmkO+qbDUhGSAjoqIbvpBStwgpgKmGrY05g1LpkpfqQmLr52QSjXkMGUuL0aWXdj+K7gxwpC0VUSpRRxH48RikX+Kq3AtgZwvCRAMJpjuxpomJSNOpqeG4qor/0dcn64iqEnHVKNZOtnh1pTBx0eZ6DiZh/3wIdxXElJPYTVL8OKa4CzAXxfECbXwAl2qsQ== rsa-key-20141216"
                   ]


andyKeys :: User -> Property UnixLike
andyKeys user = propertyList "keys for andy"
                . toProps
                . map (setupRevertableProperty . Ssh.authorizedKey user) $
                [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJu6vxDV3FUppfPgIsIcBjFSr766gEQy9HuJFIkIHg8a"
                ]


vadimKeys :: User -> Property UnixLike
vadimKeys user = propertyList "keys for vadim"
                 . toProps
                 . map (setupRevertableProperty . Ssh.authorizedKey user) $
                 [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILBuVC3RDjwGaFpk/kmXYYfb/EKk6ouhgtBwFRB8PFwX"
                 ]


scotchKeys :: User -> Property UnixLike
scotchKeys user = propertyList "keys for scotch"
                . toProps
                . map (setupRevertableProperty . Ssh.authorizedKey user) $
                [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEA6S3FSzSQsTvaSNvZVo0F6Kw0fwEmknFZrQLTRDTgia39NHCuPcEDL6pSHo4M1s5EZnzrqUrhuxQ1or0ErsF2vZFoqSvjkeT3ORF5j1AxDQJKqtP3atxSDza12aXDUTCpqYTSnmj1vvW/ognmoMW7R+IJtXSPuA4oZ5UZ7ikZakD+rK/88kmoJP4obH1cW7jof3UYNliN3KFrAJpz3svJF/CqokZilw6VaKSdufoqICTVkjgbiEBuBeB1G90MEHek64j9nZdVr4XQN1pBh/wOavw70wI5i8x3WvHxGyaEaAPteuZ0yPuTQVuxrJbPEVnGgjFKYE8N2VZBMCMm027GkQ== rsa-key-20171113"
                ]


tevoKeys :: User -> Property UnixLike
tevoKeys user = propertyList "keys for tevo"
                . toProps
                . map (setupRevertableProperty . Ssh.authorizedKey user) $
                [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgAJf/VOiLJ8A6UpPKzkOi0DLlvpxrjzsncLr3ORsQDgTPSP7NSxlneFZGWGVWxhRp67+Q7qX/j3LMZNCOc5h8wwpPPaRoLdXEwsNdSl1pS/NHlkSrJVsNpCYdKl4C6PpF18lhWmToSOt1z73pnuECZns3J+jjh091YciaE0bmhux3nnUnAhJVb5frqsSnqccO2ZQ6Zv3TOblogIpltqJdFyg9EzpzidF93l8KTr2yl6WHyqUW3aG5Hp/PbhBjyQ1D/q/qMAwSBMv74s7LR78j9WLWEqGyH7wnVsfP946tGjlbT0egy/KQISPs9hoUHErJ7MUxhrGenlkeqFQ2PbSwcrZsP2DwInrKgCVAhV561a/jv7ElmhejB/43/VH3B0A9OErnbnpSD8DdUUjiAbDw0N3kYH2kXEmUAxjPPmtttfhfmMuaxfRWkdcgtlWZkumlspjn0YyX6l2KRu3YIsEfztmRYqyq7j9U9Wt69eusgiNeTWhz1iZu29u+d+xA0deVPNK7RuFGjCi6tG8htVOGXnNZHIWOo0WbgjAzcBGqV+zDui58tDzZ14qch1tIuD3GgZlg2rnBrgXNwunYU/dU88G9ZOLU6hKoiDLGv82rpS45ViIRpkfOyoVA6Zlnf/E2NPC9jynWTaTXiIuseHLjr3yOqyuAmRKtWubSrXFbEBeQ== rsa-key-20171113"
                ]


wigglesKeys :: User -> Property UnixLike
wigglesKeys user = propertyList "keys for wiggles"
                   . toProps
                   . map (setupRevertableProperty . Ssh.authorizedKey user) $
                   [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEA4MX6FbHohWJ/UFLvAHT3T42vyn3ZW0IZGw6lQ9ZeOHXbhUVCodqNgNYXR/i2+czK1fcqgyYRjw5BNozSKdp1kxdO5LMiFD1Iz9NMaLXqeLG/jYhfMCo7bitG3ODK/WBP1OJfqBfNuUMpn1CZCUMbl9DG+B4GNu8C7x7Psu9uJixyAUvR9HRkhYbc9dFa8hz8PiP9OzSW5vOhrM+9IhCbeJrSuzCro9zBVQXD9IJfebYkDjPohXyAURCIZT8wC3iA+o+eqfEJilnf8tvXfj9q/mJOKmdUFUxUWdU+Yfh7+WVrigKhEKkQN9A/i5JHXR0YNb31DMk4EieAJ0uk65r3OQ== rsa-key-20180302"
                   ]


aiKeys :: User -> Property UnixLike
aiKeys user = propertyList "keys for ai"
              . toProps
              . map (setupRevertableProperty . Ssh.authorizedKey user) $
              [ "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA3ELb+n+pL8fScwyS6xhnnl3v2oWAl6wDSmIgBDVuE83k/3glxljklwKE41s1Kg4JjqWGsmj6wvJ/4x0WZ6rVUH01vtG2JkcU1uRo/4pi58DFRMczWNIq5H2dgkI07Vgjq1Kf907mCH9BL0slIs4OkEPopws4cmenqnhRBepQzQ8/3ni6gvICEWmVflQy4PjiNhooB9u73oRzFe+8p/+BE0lxg6HRcBZU42u58DonB9CcQpFd8QkCYmdytv45JmcMsSYa0w6CmBA8andN7Lo/s9cL5RF0OTa/bF/28ufKbgLQi0HJ9BUZDXp46P0M2tutIQseHHV0p+0LCFlMhf0eYQ=="
              ]


waldoKeys :: User -> Property UnixLike
waldoKeys user = propertyList "keys for waldo"
                 . toProps
                 . map (setupRevertableProperty . Ssh.authorizedKey user) $
                 [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAledop4Z/eFCNcHejTaTxT+2DOLGHuRbD61vIdlTHDpZw/ssKmrxEqB1I6GYeL1DXy7V/MaPm3Eg5/1RyFAv/2/kMzYbNqgATsocfv2wEargKUpUEWoQ/LC/fr1sfQA3EAQRHvUxcScGn/VZfRvE3MfqsKQqI+mrvf+2n87EhvH3dMmtklNxNjZW8bmAUGj2sNUD1jwfPq8EASFHSBLh2vqLIKILzZbmwrQ4Y8UkJEypBuFdZvVNBT9iuLz8Gcij96X20N3Yu5/IBl08JnurLitNyGWglioLMsboBiGVMgsIikJ1fDf/90gXvH6yIf8uv7n3TvCp+5kRM69dHrHiaCQ== rsa-key-20210713"
                 ]


kaptainKeys :: User -> Property UnixLike
kaptainKeys user = propertyList "keys for kaptain"
                   . toProps
                   . map (setupRevertableProperty . Ssh.authorizedKey user) $
                   [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAvCBopAAPRfQQtQcI6QuMEXEOTNktrkvf9O9LRMA9oP9Pe0U1+wuw7w1Ur9JZ1f0dU91x2RA7OsRPpG5FHodwWyUY1DYN3bFCCFxstAMu8oPcJYWk6gBd7exaMEqw1CI8Puu5qUU/09dgtxuUbFR0vqymkwTwCADHmG5/0Ju/Jo6pP1kFZKq9XNFnhF2/4zO49dHig3ef/cIb9fBA+KpEpP55Al3iJ5LUdKfCBjdFai05n4UnleYRG/Bg4UC7xlv+ipqOl1D4PAlyhiuiEOWKhQauxU4eDsNT9AztRUxB/GzR5rVz1xeBocmkCxnrvt2P5ajEAV+51kh2C4fAluIuBw== rsa-key-20211003"
                   ]


adminKeys :: User -> Property UnixLike
adminKeys user = propertyList "admin keys" . toProps . map ($ user) $
                 [ jayessKeys
                 , robKeys
                 , scotchKeys
                 ]
